﻿using System.Drawing;
using yesUniversity;

namespace UniversityLaboratory2._6
{
    class Program
    {
        private static void Main()
        {
            ConsoleMessageGenerator.GenerateLaboratoryTitle(
                new LaboratoryInfo(
                    "Бродский Егор Станиславович",
                    "ИР-133Б",
                    6,
                    "Багатовимірні масиви."
                )
            );
            Console.WriteLine();
            TaskChooser.Run(new TaskInfo[]
            {
                new("Завдання 1", () =>
                {
                    var sizeX = 20;
                    var sizeY = 30;
                    var array = Utils.GenerateRandomArray(sizeX, -100, 100, sizeY);
                    var greatestArray = new Point[30];

                    for (int i = 0; i < sizeX; i++)
                    {
                        var maxIndex = 0;
                        for (int j = 0; j < sizeY; j++)
                        {
                            if (array[i, j] > array[i, maxIndex])
                            {
                                maxIndex = j;
                            }
                        }

                        greatestArray[i] = new Point(i, maxIndex);
                    }

                    Console.WriteLine("Масив: ");
                    for (int i = 0; i < sizeY; i++)
                    {
                        for (int j = sizeX -1; j >= 0; j--)
                        {
                            var maxPoint = greatestArray[j];
                            if (i == maxPoint.Y)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.Write($" {array[j, i].ToString(),4}");
                                Console.ResetColor();
                            }
                            else
                            {
                                Console.Write($" {array[j, i].ToString(),4}");
                            }
                        }

                        Console.WriteLine();
                    }
                    
                    Console.WriteLine("Максимальні елементи масиву: ");
                    Console.WriteLine(string.Join(", ",
                        greatestArray.Select(item => $"{array[item.X, item.Y]}").ToArray())
                    );
                    return null;
                }),
            });
        }
    }
}