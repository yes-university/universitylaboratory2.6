﻿namespace   yesUniversity;

public static class Utils
{
    public static int ToInt(string? text)
    {
        var minus = false;
        var parsed = int.Parse(
            string.Join("",
                (new string[] {"0"})
                .Concat(
                    text?.ToCharArray()
                        .Select((c) =>
                        {
                            var str = c.ToString();
                            if (str != "-") return str;
                            minus = true;
                            return "0";
                        })
                        .Where((s =>
                                {
                                    try
                                    {
                                        int.Parse(s);
                                        return true;
                                    }
                                    catch (Exception e)
                                    {
                                        return false;
                                    }
                                }
                            ))
                    ?? Array.Empty<string>()
                )
            )
        );
        return minus ? -parsed : parsed;
    }

    public static float ToFloatInt(string? text)
    {
        return float.Parse(text ?? "0");
    }

    public static double ToDoubleInt(string? text)
    {
        return Convert.ToDouble(text ?? "0");
    }
    public static int[] GenerateRandomArray(int size, int min, int max)
    {
        var array = new int[size];
        var rnd = new Random();
        for (var i = 0; i < size; i++)
        {
            array[i] = rnd.Next(min, max);
        }
        return array;
    }
    public static int[,] GenerateRandomArray(int size, int min, int max, int size2)
    {
        var array = new int[size, size2];
        var rnd = new Random();
        for (var i = 0; i < size; i++)
        {
            for (var j = 0; j < size2; j++)
            {
                array[i, j] = rnd.Next(min, max);
            }
        }
        return array;
    }
}